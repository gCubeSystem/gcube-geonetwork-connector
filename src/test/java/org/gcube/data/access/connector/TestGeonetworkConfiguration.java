package org.gcube.data.access.connector;

import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.util.Iterator;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.gcube.data.access.connector.rest.entity.AccessibleCredentialsEntity;
import org.gcube.data.access.connector.rest.entity.SDIEntity;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

@Ignore
public class TestGeonetworkConfiguration {
	
	private static String CONTEXT_MANAGER = "CONTEXT_MANAGER";
	private static String CONTEXT_USER = "CONTEXT_USER";
	private static Logger logger = LoggerFactory.getLogger("TestGeonetworkConfiguration");
	
	public static void main(String[] args) {
		try {
			logger.info("TestGeonetworkConfiguration");
			
			String url = "http://sdi-d-d4s.d4science.org/sdi-service/gcube/service/SDI?gcube-token=feda0617-cd9d-4841-b6f0-e047da5d32ed-98187548";
			
			CredentialsProvider credsProvider = new BasicCredentialsProvider();
	        credsProvider.setCredentials(new AuthScope("proxy.eng.it", 3128), new UsernamePasswordCredentials("usr", "pass"));

	        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
	        HttpHost proxy = new HttpHost("proxy.eng.it", 3128);
	        RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
	        HttpGet httpget = new HttpGet(url);
	        httpget.setConfig(config);
			
			logger.warn("Executing request " + httpget.getRequestLine() + " to " + url);
			
	        CloseableHttpResponse resp = httpclient.execute(httpget);
	        String json = EntityUtils.toString(resp.getEntity());
	        logger.warn("JSON response: \n" + json);
            
            Gson gson = new Gson();
            SDIEntity response =(SDIEntity) gson.fromJson(json, SDIEntity.class);
            
            logger.warn("SDIEntity: \n" + response.toString());
            
	        logger.info("Credentials AccessType " + getAccessibleCredentials().getAccessType());
		} catch (Exception e) {
			logger.error("Error in TestGeonetworkConfiguration: " + e.getMessage());
		}
	}
	
	public static AccessibleCredentialsEntity getAccessibleCredentials() {
		try {
			String url = "http://sdi-d-d4s.d4science.org/sdi-service/gcube/service/SDI?gcube-token=feda0617-cd9d-4841-b6f0-e047da5d32ed-98187548";
					
			CredentialsProvider credsProvider = new BasicCredentialsProvider();
	        credsProvider.setCredentials(new AuthScope("proxy.eng.it", 3128), new UsernamePasswordCredentials("usr", "pass"));

	        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
	        HttpHost proxy = new HttpHost("proxy.eng.it", 3128);
	        RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
	        HttpGet httpget = new HttpGet(url);
	        httpget.setConfig(config);
		
			String host = "geonetwork1-d-d4s.d4science.org";
			logger.warn("Executing request " + httpget.getRequestLine() + " to " + url);
		
			CloseableHttpResponse resp = httpclient.execute(httpget);
	        String json = EntityUtils.toString(resp.getEntity());
	        logger.warn("JSON response: \n" + json);
	        
	        Gson gson = new Gson();
            SDIEntity response =(SDIEntity) gson.fromJson(json, SDIEntity.class);
            			
			String baseEndpoint = response.getGeonetworkConfiguration().get(0).getBaseEndpoint();
			AccessibleCredentialsEntity result = new AccessibleCredentialsEntity();
	
			if (baseEndpoint.contains(host)){		
				List<AccessibleCredentialsEntity> credentials = response.getGeonetworkConfiguration().get(0).getAccessibleCredentials();
				Iterator<AccessibleCredentialsEntity> iter = credentials.iterator();
				while (iter.hasNext()){
					AccessibleCredentialsEntity entity = iter.next();
					if (CONTEXT_MANAGER.equals(entity.getAccessType())){
						return entity;
					}	
					if ((result.getAccessType() == null) && CONTEXT_USER.equals(entity.getAccessType())){
						result = entity;
					}
				}
			}
			return result;
		} catch (Exception e) {
			logger.error("Error in getAccessibleCredentials: " + e.getMessage());
		}
		return null;
	}

}

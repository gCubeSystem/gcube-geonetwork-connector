package org.gcube.data.access.connector;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Ignore
public class Test {

	private static Logger logger = LoggerFactory.getLogger("Test");
	
	public static void main(String[] args) {
		try {
			logger.info("Test");
			
			String url = "https://geonetwork-1.dev.d4science.org/geonetwork/srv/api/0.1/me";
			
			String token = "admin:admin";
		
			CredentialsProvider credsProvider = new BasicCredentialsProvider();
	        credsProvider.setCredentials(new AuthScope("proxy.eng.it", 3128), new UsernamePasswordCredentials("usr", "pass"));

	        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
	        HttpHost proxy = new HttpHost("proxy.eng.it", 3128);
	        RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
	        HttpGet httpget = new HttpGet(url);
	        httpget.setConfig(config);
	        httpget.setHeader("Content-type", "application/json");
	        httpget.setHeader("Authorization", "Basic " + Base64.encodeBase64String(token.getBytes()));
	        logger.warn("Executing request " + httpget.getRequestLine() + " to " + url);
	        
	        CloseableHttpResponse response = httpclient.execute(httpget);
	        logger.warn("response code: "+ response.getStatusLine().getStatusCode());
	        
			String json = EntityUtils.toString(response.getEntity());
			logger.warn("JSON response: \n" + json);
            	        
	        logger.info("Result - status ("+ response.getStatusLine().getStatusCode() + ") has body: " + response.getEntity());
	        
		} catch (Exception e) {
			logger.error("Error in Test: " + e.getMessage());
		}
	}

}

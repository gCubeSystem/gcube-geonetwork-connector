package org.gcube.data.access.connector;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.gcube.data.access.connector.rest.entity.AccessibleCredentialsEntity;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.json.JSONObject;

@Ignore
public class TestRest {

	private static Logger logger = LoggerFactory.getLogger("TestRest");
	
	public static void main(String[] args) throws Exception {
		
		logger.info("TestRest");

		String url = "sdi-d-d4s.d4science.org";
		
		//http://sdi-d-d4s.d4science.org/sdi-service/gcube/service/GeoNetwork/credentials/geonetwork-sdi.dev.d4science.org?gcube-token=feda0617-cd9d-4841-b6f0-e047da5d32ed-98187548
		
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(new AuthScope("proxy.eng.it", 3128), new UsernamePasswordCredentials("usr", "pass"));

        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
        try {
            HttpHost target = new HttpHost(url);
            HttpHost proxy = new HttpHost("proxy.eng.it", 3128);

            RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
            HttpGet httpget = new HttpGet("/sdi-service/gcube/service/GeoNetwork/credentials/geonetwork-sdi.dev.d4science.org?gcube-token=feda0617-cd9d-4841-b6f0-e047da5d32ed-98187548");
            httpget.setConfig(config);

            logger.warn("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
            CloseableHttpResponse response = httpclient.execute(target, httpget);
            try {
                logger.warn("Status Code: "+response.getStatusLine().getStatusCode());
                
                String json = EntityUtils.toString(response.getEntity());
                logger.warn("JSON response: \n" + json);
                JSONObject jsonObject = JSONObject.fromObject(json);
                
                AccessibleCredentialsEntity ac = (AccessibleCredentialsEntity) JSONObject.toBean(jsonObject, AccessibleCredentialsEntity.class);
              
               	logger.info("Credentials Entity Username"+ac.getUsername());
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
    }

}

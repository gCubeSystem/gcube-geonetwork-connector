package org.gcube.data.access.connector.rest;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.gcube.data.access.connector.rest.entity.AccessibleCredentialsEntity;
import org.gcube.data.access.connector.rest.entity.SDIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import net.sf.json.JSONObject;

public class GCubeRestClient {
	
	private static String CONTEXT_MANAGER = "CONTEXT_MANAGER";
	private static String CONTEXT_USER = "CONTEXT_USER";

	private Logger logger = LoggerFactory.getLogger(GCubeRestClient.class);
	
	public AccessibleCredentialsEntity getAccessibleCredentials(String url) {
		
		CloseableHttpClient httpclient = HttpClients.custom().build();
	
		RequestConfig config = RequestConfig.custom().build();
        HttpGet httpget = new HttpGet(url);
        httpget.setConfig(config);
        
        logger.debug("Executing request " + httpget.getRequestLine() + " to " + url);
        
		try {
			
			CloseableHttpResponse response = httpclient.execute(httpget);
			String json = EntityUtils.toString(response.getEntity());
			
			logger.debug("JSON response: \n" + json);

			JSONObject jsonObject = JSONObject.fromObject(json);
			return (AccessibleCredentialsEntity) JSONObject.toBean(jsonObject, AccessibleCredentialsEntity.class);
		} catch (Exception e) {
			logger.error("Error in getAccessibleCredentials() method: " + e.getMessage());
			return new AccessibleCredentialsEntity();
		}
	}
	
	public AccessibleCredentialsEntity getGeneralAccessibleCredentials(String url, String host) {
		
		logger.debug("REST call to URL: " + url);
		
		CloseableHttpClient httpclient = HttpClients.custom().build();
		
		HttpHost target = new HttpHost(host);
		
		RequestConfig config = RequestConfig.custom().build();
        HttpGet httpget = new HttpGet(url);
        httpget.setConfig(config);
        
        logger.debug("Executing request " + httpget.getRequestLine() + " to " + url);
		
		try {
			
			CloseableHttpResponse resp = httpclient.execute(target, httpget);
			String json = EntityUtils.toString(resp.getEntity());
			logger.debug("JSON response: \n" + json);
                        
            Gson gson = new Gson();
            SDIEntity response =(SDIEntity) gson.fromJson(json, SDIEntity.class);
            
            String baseEndpoint = response.getGeonetworkConfiguration().get(0).getBaseEndpoint();
            AccessibleCredentialsEntity result = new AccessibleCredentialsEntity();

			if (baseEndpoint.contains(host)){	
				
				List<AccessibleCredentialsEntity> credentials = response.getGeonetworkConfiguration().get(0).getAccessibleCredentials();
				Iterator<AccessibleCredentialsEntity> iter = credentials.iterator();		
				while (iter.hasNext()){
					AccessibleCredentialsEntity entity = iter.next();
					if (CONTEXT_MANAGER.equals(entity.getAccessType())){
						result = entity;
					}
					if ((result.getAccessType() == null) && CONTEXT_USER.equals(entity.getAccessType())){
						result = entity;
					}
				}	
			}
			
			return result;
		} catch (Exception e) {
			logger.error("Error in getGeneralAccessibleCredentials() method: " + e.getMessage());
			return new AccessibleCredentialsEntity();
		}
	}
	
	
	public AccessibleCredentialsEntity getAccessibleCredentialsHttp(String host, String get) {
		
		CloseableHttpClient httpclient = HttpClients.custom().build();
		
		HttpHost target = new HttpHost(host);
		
		RequestConfig config = RequestConfig.custom().build();
        HttpGet httpget = new HttpGet(get);
        httpget.setConfig(config);
        
        logger.debug("Executing request " + httpget.getRequestLine() + " to " + get);

        try {
			CloseableHttpResponse response = httpclient.execute(target, httpget);
			String json = EntityUtils.toString(response.getEntity());
			logger.debug("JSON response: \n" + json);
            JSONObject jsonObject = JSONObject.fromObject(json);
            
            AccessibleCredentialsEntity ac = (AccessibleCredentialsEntity) JSONObject.toBean(jsonObject, AccessibleCredentialsEntity.class);
            logger.info("Credentials Entity Username: "+ac.getUsername());
            return ac;
		} catch (IOException e) {

			logger.error("Error in getAccessibleCredentialsHttp() method: " + e.getMessage());
			return new AccessibleCredentialsEntity();
		}
	}
}

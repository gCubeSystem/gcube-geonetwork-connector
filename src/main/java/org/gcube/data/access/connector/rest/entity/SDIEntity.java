package org.gcube.data.access.connector.rest.entity;

import java.util.List;

public class SDIEntity {

	private String contextName;
	private List<InstancesEntity> geonetworkConfiguration;
	private List<GeoserverClusterConfigurationEntity> geoserverClusterConfiguration;

	public String getContextName() {
		return contextName;
	}

	public void setContextName(String contextName) {
		this.contextName = contextName;
	}

	public List<InstancesEntity> getGeonetworkConfiguration() {
		return geonetworkConfiguration;
	}

	public void setGeonetworkConfiguration(List<InstancesEntity> geonetworkConfiguration) {
		this.geonetworkConfiguration = geonetworkConfiguration;
	}

	public List<GeoserverClusterConfigurationEntity> getGeoserverClusterConfiguration() {
		return geoserverClusterConfiguration;
	}

	public void setGeoserverClusterConfiguration(List<GeoserverClusterConfigurationEntity> geoserverClusterConfiguration) {
		this.geoserverClusterConfiguration = geoserverClusterConfiguration;
	}

	@Override
	public String toString() {
		return "SDI [contextName=" + contextName + ", GeonetworkConfiguration=" + geonetworkConfiguration
				+ ", GeoserverClusterConfiguration=" + geoserverClusterConfiguration + "]";
	}
}

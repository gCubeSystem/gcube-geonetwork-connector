# Changelog for gcube-geonetwork-connector

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.0.0]

- set boolean GENERAL value to true
- change maven-smartgears-bom with LATEST version to gcube-smartgears-bom 2.5.0

## [v0.1.0-SNAPSHOT] - 2023-06-13

- ported to git
